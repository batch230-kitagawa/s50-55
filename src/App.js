//import logo from './logo.svg';
//import Banner from './components/Banner'
//import Highlights from './components/Highlights'
import './App.css';
import { Container } from 'react-bootstrap';
//import { Fragment } from 'react'; // React fragments allows us to return multiple elements
import { BrowserRouter as Router } from 'react-router-dom'; 
import { Route, Routes } from 'react-router-dom'

import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import NotFound from './pages/NotFound'
import CourseView from './pages/CourseView'

import { UserProvider } from './UserContext'
import { useState, useEffect, useContext } from 'react'

import Settings from './pages/Settings'

function App() {
  const [user, setUser] = useState({
    email: localStorage.getItem('email')
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
    <Router> 
     
      <Container Fluid>
        <AppNavbar />
        <Routes>
        <Route path="/" element={<Home />} />
            <Route path="/courses" element={<Courses />} />
            <Route path="/courses/:courseId" element={<CourseView />} />
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} /> 
            <Route path="/settings" element={<Settings />} /> 
            <Route path="*" element={<NotFound />} /> 
        </Routes>
      </Container>
    </Router>
    </UserProvider>
  /*
<Router>
<Routes>
<Route path="/yourDesiredPath" element {<component />} />
</Routes>
</Router>
  */
 );
}

export default App;
