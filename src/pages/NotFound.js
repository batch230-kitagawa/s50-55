//import Container from 'react-bootstrap/Container';

/*
export default function ErrorPage (){
    return (
        <Container fluid>
            <h1>Ooops! Page Not Found!</h1>
            <p>Go back to the <a href='./'>Home</a></p>

        </Container>
    )
}
*/
/*
import { Navigate, useNavigate } from 'react-router-dom'
import { Button, Row } from 'react-bootstrap'

function navigateToHome(navigate) {
    navigate('/')
}

export default function ErrorPage(){
    const navigate = useNavigate()

    return(
        <>
            <Row className='fullpage d-flex align-items-center'>
                <div className='m-auto mt-5 justify-content-center'>
                    <div className='d-flex'>
                    <h1 className='pageHeader m-auto'>Oops! Page not found!</h1>
                    </div>
                    <div className='d-flex justify-content-center mt-2'>
                    <Button className='btn-primary' onClick={() => navigateToHome(navigate)} >Home</Button>
                    </div>
                </div>    
            </Row>
        </>
    )
}
*/

import Banner from '../components/Banner';

export default function Error() {

	// We created an object variable named "data" to contains the properties we want to sent in the Banner component.
	const data = {
		title: "404 - Not found",
		content: "The page you are looking for cannot be found",
		destination: "/",
		label: "Back home"
	}

	return (
		<Banner data={data}/>
	)
}
