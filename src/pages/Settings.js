import { useContext } from "react"
import { Navigate } from 'react-router-dom'
import UserContext from "../UserContext"

export default function Settings() {
    const { user, setUser} = useContext(UserContext)

    return(
        (user.email !== null)?
        <h3>Settings Page</h3>
        :
        <Navigate to='/login' />
    )
}