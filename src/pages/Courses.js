import { Fragment } from 'react';
import { Navigate } from "react-router-dom";
import { useEffect, useState, useContext } from "react";
// import coursesData from '../data/coursesData'; // mock database removed
import CourseCard from '../components/CourseCard';
import UserContext from '../UserContext';

export default function Courses(){

	/*
		console.log("Contents of coursesData: ");
		console.log(coursesData);
		console.log(coursesData[0]);
	*/
	const { user } = useContext(UserContext);

	const [courses, setCourses] = useState([]);
	//Function to fetch our courses data. The reason we have this in a function instead of directly in a useEffect hook is so that it can be reused and invoked ONLY when a page needs to re-render, instead of constantly (which causes a memory leak due to infinite looping)

	useEffect(() =>{
		// Will retrieve all the active courses
		fetch(`${process.env.REACT_APP_API_URL}/courses`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setCourses(data.map(course =>{
				return(
					<CourseCard key={course._id} courseProp={course}/>
				);
			}));
		})
	}, []);



	// array method to display all courses
	// const courses = coursesData.map(course => {
	// 	return (
	// 		<CourseCard key={course.id} courseProp={course} /> 
	// 	)
	// })
	// return(
	// 	<Fragment>
	// 		<CourseCard courseProp={coursesData[0]} />
	// 	</Fragment>
	// )

	return(
		(user.isAdmin)
		?
			<Navigate to="/admin" />
		:	
		<>
			<h1>Courses</h1>
			{courses}
		</>
	)

}
