import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Register(){

	const {user} = useContext(UserContext);

    //State hooks to store the values of input fields
    const [email, setEmail] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNumber, setMobileNumber] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    // Check if values are successfully binded
    console.log(email);
    console.log(password1);
    console.log(password2);

    function registerUser(event){

        // Prevents page redirection via form submission
        event.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`,{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email:email
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if (data === true) {
                Swal.fire({
                    title: 'Duplicate email found!',
                    icon: 'error',
                    text: 'Please provide another email to complete the registration.'
                })
            } 
            else {
                fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                    method: 'POST',
                    headers:{
                        'COntent-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        mobileNumber: mobileNumber,
                        password: password1
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data);

                    if (data === true) {
                        Swal.fire({
                            title: 'Registration Successful',
                            icon: 'success',
                            text: 'Welcome to Zuitt!'
                        })

                        // Clear input fields
                        setFirstName('');
                        setLastName('');
                        setMobileNumber('');
                        setEmail('');
                        setPassword1('');
                        setPassword2('');
                    }
                    else{
                        Swal.fire({
                        title: 'Something went wrong',
                        icon: 'error',
                        text: 'Please try again'
                        })
                    }
                })
            }
        })
        
    }

    useEffect(() => {
        if((email !== '' && firstName !== '' && lastName !== '' && mobileNumber.length >= 11 && password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }
    }, [email, firstName, lastName, mobileNumber, password1, password2])

    // const { user, setUser } = useContext(UserContext);

    return(
        (user.id !== null)?
        <Navigate to ="/courses" />
        :
        <Form onSubmit={(event) => registerUser(event)}>
            
            <Form.Group controlId="userFirstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Enter first name" 
                    value = {firstName}
                    onChange = {event => setFirstName(event.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="userLastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Enter last name" 
                    value = {lastName}
                    onChange = {event => setLastName(event.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    value = {email}
                    onChange = {event => setEmail(event.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="userMobileNumber">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
                    type="number" 
                    placeholder="Enter Mobile Number" 
                    value = {mobileNumber}
                    onChange = {event => setMobileNumber(event.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value = {password1}
                    onChange = {event => setPassword1(event.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Verify Password" 
                    value = {password2}
                    onChange = {event => setPassword2(event.target.value)}
                    required
                />
            </Form.Group>

            <div className='pt-3'>
                { isActive ?
                    <Button variant="primary" type="submit" id="submitBtn">
                        Submit
                    </Button>
                    :
                    <Button variant="primary" type="submit" id="submitBtn" disabled>
                        Submit
                    </Button>
                }
            </div>
        </Form>
    )
}
